#!/bin/bash -i
#make sure it's -i (interactive shell) so that it picks up my .bashrc which has paths to kubectl and istioctl

#export PATH="/usr/local/bin:/usr/bin:/Users/mworthington/software/bin/:$PATH"
BIT_BAR_MENU_TITLE="K8s"

SCRIPTS_DIR="/Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/"
source $SCRIPTS_DIR/config.cfg 


if [ -e $SCRIPTS_DIR/k8s-poll-active.txt ]
then
	instances=0
	[ $instances -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${BIT_BAR_MENU_TITLE}($instances) | color=$menubar_running_color"

	echo "---"
	echo "Click to Pause | bash=$SCRIPTS_DIR/stop-k8s-poll.sh terminal=false refresh=true"
	echo "---"

else
	echo "$BIT_BAR_MENU_TITLE(p) | color=$menubar_paused_color"

	echo "---"
	echo "Paused (Click to Continue) | bash=$SCRIPTS_DIR/start-k8s-poll.sh terminal=false refresh=true"

	exit 0
fi


function istio_object() {
    P_TYPE=$1

	IMAGES="$(kubectl get $P_TYPE --all-namespaces | tail -n +2 | sort)"
	#IMAGES=`kubectl get $P_TYPE --all-namespaces | tail -n +2 | sort`
	
	if [ -z "$IMAGES" ]; 
	then
		echo "No images"
	else
		#echo "$SYM Image List | bash=$(which docker) param1=images terminal=true"

		LAST_IMAGE=$(echo "$IMAGES" | tail -n1 )
		#echo "${IMAGES}" | while read -r IMAGE; 
		while read -r IMAGE
		do
			IMAGE_NAMESPACE=$(echo "$IMAGE" | awk '{print $1}')
			IMAGE_NAME=$(echo "$IMAGE" | awk '{print $2}')

			SYM="├ 💻 "
			SYM_LOGS="├       "
			SYM_END="├       "

			if [ "$IMAGE" = "$LAST_IMAGE" ]; 
			then 
#				SYM="└ 💻 ";
				SYM="├ 💻 "
				SYM_LOGS="├       "
				SYM_END="└       "
			fi
			
			echo "-- $SYM $IMAGE_NAMESPACE -  $IMAGE_NAME | bash='istioctl' param1='get' param2=$P_TYPE param3=$IMAGE_NAME param4='--output' param5='yaml' param6='--namespace' param7=$IMAGE_NAMESPACE terminal=true"
			echo "---- $SYM_END Delete | bash='istioctl' param1=delete param2=$P_TYPE param3=$IMAGE_NAME param4='--namespace' param5=$IMAGE_NAMESPACE terminal=false refresh=true"
		done <<< "${IMAGES}" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered
	fi
}


#echo "Get Nodes | bash=$(which kubectl) param1=get param2=nodes terminal=true"
echo "Get Nodes | bash='kubectl' param1='get' param2='nodes' param3='--all-namespaces' terminal=true"
echo "-- First Node's Address | bash='kubectl' param1='get' param2='nodes' param3='-o' param4='jsonpath={.items[0].status.addresses[0].address}' terminal=true"
echo "Get Pods | bash='kubectl' param1='get' param2='pods' param3='--all-namespaces' terminal=true"
echo "Get Services | bash='kubectl' param1='get' param2='services' param3='--all-namespaces' terminal=true"
echo "Get Replica Sets | bash='kubectl' param1='get' param2='replicaset' param3='--all-namespaces' terminal=true"
echo "Get Deployments | bash='kubectl' param1='get' param2='deployments' param3='--all-namespaces' terminal=true"
echo "-- Describe Deployment | bash='kubectl' param1='describe' param2='deployment' param3='--all-namespaces' terminal=true"

echo "---"

echo "Get All | bash='kubectl' param1='get' param2='all' param3='--all-namespaces' param4='--include-uninitialized' param5='--output=wide' terminal=true"
echo "Get Namespaces | bash='kubectl' param1='get' param2='namespaces' terminal=true"

echo "---"

echo "Cluster Info | bash='kubectl' param1='cluster-info' terminal=true"
echo "Storage Classes | bash='kubectl' param1='get' param2='storageclasses' param3='--all-namespaces' terminal=true"
echo "Service Accounts | bash='kubectl' param1='get' param2='serviceaccounts' param3='--all-namespaces' terminal=true"
echo "Cluster Roles | bash='kubectl' param1='get' param2='ClusterRole' param3='--all-namespaces' terminal=true"
echo "Cluster Role Bindings | bash='kubectl' param1='get' param2='ClusterRoleBinding' param3='--all-namespaces' terminal=true"
echo "Custom Resource Definitions | bash='kubectl' param1='get' param2='customresourcedefinitions' param3='--all-namespaces' terminal=true"

echo "---"

SYM="├ 💻 "


echo "Istio"
echo "$SYM Get All | bash='istioctl' param1='get' param2='all' param3='--all-namespaces' terminal=true"
echo "$SYM Gateways | bash='istioctl' param1='get' param2='gateway' param3='--all-namespaces' terminal=true"
istio_object Gateway

echo "$SYM Virtual Services | bash='istioctl' param1='get' param2='VirtualService' param3='--all-namespaces' terminal=true"
istio_object VirtualService

echo "$SYM Destination Rules | bash='istioctl' param1='get' param2='DestinationRule' param3='--all-namespaces' terminal=true"
istio_object DestinationRule


echo "---"

echo "Kubernetes Dashboard"
echo "-- Start Proxy | bash='kubectl' param1='proxy' terminal=true"
echo "-- http 8001 | href=http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/"

echo "Hello Minikube"
echo "-- Deployment"
echo "---- Start Echo Server Deployment | bash='kubectl' param1='run' param2='hello-minikube' param3='--image=k8s.gcr.io/echoserver:1.10' param4='--port=8080' terminal=true"
echo "---- Delete Deployment | bash='kubectl' param1='delete' param2='deployment' param3='hello-minikube' terminal=true"
echo "-- Service"
echo "---- Expose NodePort | bash='kubectl' param1='expose' param2='deployment' param3='hello-minikube' param4='--type=NodePort' terminal=true"
echo "---- http 30700 | href=http://localhost:30700/"
echo "---- Delete NodePort | bash='kubectl' param1='delete' param2='service' param3='hello-minikube' terminal=true"
echo "---- ---"
echo "---- Expose LoadBalancer | bash='kubectl' param1='expose' param2='deployment' param3='hello-minikube' param4='--type=LoadBalancer' param5='--name=hmk-lb' terminal=true"
echo "---- http 8080 | href=http://localhost:8080/"
echo "---- Delete LoadBalancer | bash='kubectl' param1='delete' param2='service' param3='hmk-lb' terminal=true"

