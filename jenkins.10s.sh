#!/bin/bash
source /Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/config.cfg 

#ps aux | grep "jenkins-.*\.war$" | grep -v grep >/dev/null 2>&1
#rc=$?
#echo "Jenkins: $rc"
#echo "---"
#[ $rc -gt 0 ] && echo "Jenkins | color=red" || echo "Jenkins | color=green"
#[ $? -gt 0 ] && echo "Jenkins | color=red" || echo "Jenkins | color=green"

war_instances=`ps aux | grep "jenkins.*\.war" | grep -v grep | grep -v runme.sh | wc -l | tr -d '[[:space:]]'`
hpi_instances=`ps aux | grep "hpi:run" | grep -v grep | grep -v runme.sh | wc -l | tr -d '[[:space:]]'`

instances=$((war_instances + hpi_instances))
[ $instances -lt 1 ] && echo "Jenkins | color=$menubar_stopped_color" || echo "Jenkins($instances) | color=$menubar_running_color"

#mvn hpi:run
while read line
do
	# the while loop executes for some reason when line is zero (i.e. if there are no processes running)
    if [ -n "$line" ] 
    then
	    instance_pid=`echo $line | awk '{print $2}'`

	    # Think about replacing this across the board for pulling out -D values
	    #    https://www.linuxquestions.org/questions/programming-9/how-to-print-a-column-starting-with-a-string-using-awk-4175546313/
	    instance_full_dir=`echo $line | grep -o "\-Dmaven.multiModuleProjectDirectory=[^[:space:]]*" | cut -d = -f 2`
	    instance_dir=`basename $instance_full_dir`
	    instance_port=`echo $line | grep -o "\-Djetty.port=[^[:space:]]*" | cut -d = -f 2`

		echo "---"

		echo "$instance_dir ($instance_pid) | color=green"
	    echo "http: $instance_port | href=http://localhost:$instance_port"
	    echo "http: $instance_port (env-vars) | href=http://localhost:$instance_port/env-vars.html/"
	fi
done <<< "$(ps aux | grep "hpi:run" | grep -v grep | grep -v runme.sh | sort)" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered

instance_dirs=""

#jenkins.war
while read line
do
	# the while loop executes for some reason when line is zero (i.e. if there are no processes running)
    if [ -n "$line" ] 
    then
	    echo $line | grep "bitbar-pwd" > /dev/null 2>&1
	    has_bitbar=$?

	    instance_pid=`echo $line | awk '{print $2}'`
	    instance_dir=`echo $line | awk '{str=$12; sub("-Dbitbar-pwd=", "", str); print str}'`
	    instance_dirs="$instance_dirs $instance_dir | "

		echo "---"

		if [ $has_bitbar -eq 0 ]
	    then
			echo "$instance_dir ($instance_pid) | color=green"

			#check if the port is set
			grep "^HTTP_PORT=" $instance_dir/runme.sh >/dev/null 2>&1
			rc=$?
			if [ $rc -eq 0 ]
			then
				#PORT="$(grep port $iq_dir/config.yml | grep -v '#' | grep -v org.)"
				#echo "${PORT}"
				for i in `grep "^HTTP_PORT=" $instance_dir/runme.sh | cut -f 2- -d = `
				do 
				    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
				    #echo "port: $i | bash='open http://localhost:$i && exit'"
				    echo "http: $i | href=http://localhost:$i"
	    			echo "http: $i (env-vars) | href=http://localhost:$i/env-vars.html/"
				done
			else
				#default port
				i="8080"

    		    echo "http (default): $i | href=http://localhost:$i"
    			echo "http (default): $i (env-vars) | href=http://localhost:$i/env-vars.html/"
    		fi


			#echo "---"
			echo "Log Dir | bash='cd $instance_dir/'"
			echo "-- jenkins.log | bash='cd $instance_dir/ && tail -F jenkins.log'"

			#echo "---"
		else
			echo "No Dir for PID: $instance_pid"
		fi

		#echo "Stop Jenkins | color=purple bash=$plugin_dir/stop-jenkins.sh terminal=false"
		#echo "Stop Jenkins | color=purple bash='kill $instance_pid && exit' terminal=true" #i can't figure out why a command with spaces won't run when terminal=false, so true and exit
		#echo "Restart Jenkins | color=purple bash='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/runme.sh > /dev/null 2>&1 & exit' terminal=true refresh=true"

		echo "Stop Jenkins | color=purple bash='/bin/bash' param1='-c' param2='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done' terminal=false refresh=true"
		echo "Restart Jenkins | color=purple bash='/bin/bash' param1='-c' param2='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/runme.sh > /dev/null 2>&1 &' terminal=false refresh=true"
	fi
done <<< "$(ps aux | grep "jenkins.*\.war" | grep -v grep | grep -v runme.sh | sort)" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered

echo "---"
#	echo "instance_dirs: $instance_dirs"
#echo "---"

for (( i=0; i < ${#jenkins_dirs[@]}; i++ ))
do

	#this_base_dir=`dirname ${jenkins_dirs[$i]}`
	this_base_dir=${jenkins_dirs[$i]}

#	echo "this_base_dir: $this_base_dir"
	echo $instance_dirs | grep "$this_base_dir |" > /dev/null 2>&1
	if [ $? -gt 0 ]
	then
		#echo "Start Jenkins | color=#123def bash=$plugin_dir/start-jenkin.sh terminal=false" 
		#echo "Start Jenkins (${jenkins_dirs[$i]}) | color=#123def bash='${jenkins_dirs[$i]}/runme.sh > /dev/null 2>&1 & exit' terminal=true"

		#testing because docker needs a login shell to work with jenkins
		#echo "Start Jenkins bitbar f (${jenkins_dirs[$i]}) | color=#123def bash='/bin/bash' param1='-l' param2='-c' param3='${jenkins_dirs[$i]}/runme.sh > /dev/null 2>&1 &' terminal=false refresh=true"
		#echo "Start Jenkins script f (${jenkins_dirs[$i]}) | color=#123def bash='/bin/bash' param1='-c' param2='${jenkins_dirs[$i]}/runme.sh' terminal=false refresh=true"
		echo "Start Jenkins (${jenkins_dirs[$i]}) | color=#123def bash='/bin/bash' param1='-c' param2='${jenkins_dirs[$i]}/runme.sh > /dev/null 2>&1 &' terminal=false refresh=true"
	fi
done
