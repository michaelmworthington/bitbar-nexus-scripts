#!/usr/bin/env bash
#
# <bitbar.title>Docker Status</bitbar.title>
# <bitbar.version>v1.1</bitbar.version>
# <bitbar.author>Manoj Mahalingam</bitbar.author>
# <bitbar.author.github>manojlds</bitbar.author.github>
# <bitbar.image>https://cloud.githubusercontent.com/assets/191378/12255368/1e671b32-b919-11e5-8166-6d975396f408.png</bitbar.image>
# <bitbar.desc>Displays the status of docker machines and running containers</bitbar.desc>
# <bitbar.dependencies>shell,docker,docker-machine</bitbar.dependencies>
#
# Docker status plugin
# by Manoj Mahalingam (@manojlds)
#
# Displays the status of docker machines and running containers
#
# Docker Commands Used
#   * docker ps
#     * -q
#     * -f status=exited
#     * -a
#     * --format
#   * docker inspect <container id>
#   * docker exec -it <container id> bash
#     * -i 
#     * -t
#   * docker logs -f <container id>
#     * -f
#   * docker stop <container id>
#   * docker start <container id>
#   * docker rm <container id>
#   * docker rmi <image name>:<image tag>
#           NOTE:  you want to remove by name and tag in case the same image was pulled from multiple sources then it would have the same id, and therefore remove all if done by id
#   * docker images
#     * --format

export PATH="/usr/local/bin:/usr/bin:/Users/mworthington/software/bin/:/opt/homebrew/bin/:$PATH"
BIT_BAR_MENU_TITLE="⚓️ "
#BIT_BAR_MENU_TITLE="Docker"

#JQ_BIN=jq-osx-amd64
export JQ_BIN=jq

SCRIPTS_DIR="/Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/"
source $SCRIPTS_DIR/config.cfg 

DC_DEMO_DIR="/Users/mworthington/projects/GIT-Projects/gitlab.com/michaelmworthington/summit-demo-environment/docker-compose"
DC_WEBHOOK_SITE_DIR="/Users/mworthington/projects/GIT-Projects/github.com/fredsted/webhook.site"
NODE_WEBHOOK_DIR="/Users/mworthington/projects/GIT-Projects/github.com/michaelmworthington/simple-npm-express-webhook-listener"
COMMAND_DCU="docker-compose up -d --build"  #(optional) service-name
COMMAND_DCL='docker-compose logs -f -t' #(optional) service-name

REVERSE_PROXY_PORT=8000

IQ_FIREWALL_URL="http://host.docker.internal:$REVERSE_PROXY_PORT/rutiq/assets/index.html#/repository"
#ALT_DOCKER_REGISTRY="docker.io"
ALT_DOCKER_REGISTRY="host.docker.internal:19443"

function dcu_service_list() {
    P_SERVICES=$*
    for i in $P_SERVICES
    do
    	echo "---- $i | bash='cd $DC_DEMO_DIR && $COMMAND_DCU $i && $COMMAND_DCL $i' terminal=true"
    done
}

if [ -e $SCRIPTS_DIR/docker-poll-active.txt ]
then
	exited_containers=`docker ps -q -f status=exited | wc -l | tr -d '[[:space:]]'`
	running_containers=`docker ps -q | wc -l | tr -d '[[:space:]]'`
	#echo "$exited_containers | color=$cfg_item_stopped_color"
	#echo "$running_containers | color=$cfg_item_running_color"

	#[ $running_containers -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${BIT_BAR_MENU_TITLE}($running_containers) | color=$menubar_running_color"

	[ $running_containers -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${running_containers}${BIT_BAR_MENU_TITLE} | color=$menubar_running_color"

	IQ_SERVICES=""
	IQ_SERVICES="$IQ_SERVICES httpd"
	IQ_SERVICES="$IQ_SERVICES nexus3"
	IQ_SERVICES="$IQ_SERVICES nexus-iq-server"
	IQ_SERVICES="$IQ_SERVICES postgres"
	IQ_SERVICES="$IQ_SERVICES pgadmin4"

	SERVICES=""
	SERVICES="$SERVICES slapd-server-mock"
	SERVICES="$SERVICES slapd-server-forumsys"
	SERVICES="$SERVICES slapd-server-winclm"
	SERVICES="$SERVICES keycloak"
	SERVICES="$SERVICES prometheus"
	SERVICES="$SERVICES grafana"
	SERVICES="$SERVICES influxdb"
	SERVICES="$SERVICES fluentd-influxdb"
	SERVICES="$SERVICES simple-npm-express-webhook-listener"

	OTHER_APPS=""
	OTHER_APPS="$OTHER_APPS dependency-track"
	OTHER_APPS="$OTHER_APPS jenkins"
	OTHER_APPS="$OTHER_APPS jira"
	OTHER_APPS="$OTHER_APPS license-legal-service"
	OTHER_APPS="$OTHER_APPS nexus2"
	OTHER_APPS="$OTHER_APPS nexus2-proxy"
	OTHER_APPS="$OTHER_APPS nexus3-proxy"

	OTHER_SERVICES=""
	OTHER_SERVICES="$OTHER_SERVICES clair-db"
	OTHER_SERVICES="$OTHER_SERVICES clair-local-scan"
	OTHER_SERVICES="$OTHER_SERVICES clair-scanner"
	OTHER_SERVICES="$OTHER_SERVICES nexus-iq-success-metrics"
	OTHER_SERVICES="$OTHER_SERVICES nginx"
	OTHER_SERVICES="$OTHER_SERVICES ngrok"
	OTHER_SERVICES="$OTHER_SERVICES phpldapadmin"
	OTHER_SERVICES="$OTHER_SERVICES prometheus-jmx-exporter"
	OTHER_SERVICES="$OTHER_SERVICES prometheus-blackbox-exporter"
	OTHER_SERVICES="$OTHER_SERVICES wagoodman-dive"
	OTHER_SERVICES="$OTHER_SERVICES webhook-site"
	OTHER_SERVICES="$OTHER_SERVICES webhook-redis"
	OTHER_SERVICES="$OTHER_SERVICES laravel-echo-server" #for webhook-site
	OTHER_SERVICES="$OTHER_SERVICES webhook-listener-scripter"
	OTHER_SERVICES="$OTHER_SERVICES nexus3-proxy-webhook-listener"

	#echo "⚓️ | dropdown=false"
	echo "---"
	echo "Click to Pause | bash=$SCRIPTS_DIR/stop-docker-poll.sh terminal=false refresh=true"
    echo "Get Stats / Top | bash='docker' param1='stats' terminal=true"
    echo "Get Info | bash='docker' param1='info' terminal=true"
    echo "Clean Dangling Images | bash='docker_clean_images' terminal=true"
    echo "Connect to VM | bash='docker_connect_vm' terminal=true"
    echo "Docker Compose Demo | bash='cd $DC_DEMO_DIR' terminal=true"
    echo "-- Up (all) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU' terminal=true"
    echo "-- Up (rm-iq) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU $IQ_SERVICES' terminal=true"
    dcu_service_list $IQ_SERVICES
    echo "-- Up (services) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU $SERVICES' terminal=true"
    dcu_service_list $SERVICES
    echo "-- Up (other apps) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU $OTHER_APPS' terminal=true"
    dcu_service_list $OTHER_APPS
    echo "-- Up (other services) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU $OTHER_SERVICES' terminal=true"
    dcu_service_list $OTHER_SERVICES

    echo "-- Up (success-metrics) | bash='cd $DC_DEMO_DIR && $COMMAND_DCU nexus-iq-success-metrics && $COMMAND_DCL nexus-iq-success-metrics' terminal=true"

    echo "-- RM | bash='cd $DC_DEMO_DIR && docker-compose rm' terminal=true"
    #echo "-- Down | bash='cd $DC_DEMO_DIR && docker-compose down --timeout 240' | terminal=true"
    # i hope i can figure out the quotes, or else i have a lot of refactoring to do
    echo "-- Down | shell=cd | param1=$DC_DEMO_DIR | param2=&& | param3=docker-compose | param4=down | param5=--timeout | param6=240 | terminal=true"
    echo "-- Logs | bash='cd $DC_DEMO_DIR && docker-compose logs -f -t --tail=10' terminal=true"
    echo "-- PS | bash='cd $DC_DEMO_DIR && docker-compose ps' terminal=true"
    echo "Docker Demo"
    echo "-- IQ | bash='docker_demo_iq' terminal=true"
    echo "-- RM3 | bash='docker_demo_nxrm3' terminal=true"
    echo "-- RM2 | bash='docker_demo_nxrm2' terminal=true"
    echo "-- Firewall Containers"
    echo "---- Apt | bash='docker_demo_apt' terminal=true"
    echo "---- Bower | bash='docker_demo_bower' terminal=true"
    echo "---- CocoaPods | bash='docker_demo_cocoapods' terminal=true"
    echo "---- Conan | bash='docker_demo_conan' terminal=true"
    echo "---- Conda | bash='docker_demo_conda' terminal=true"
    echo "---- Docker | bash='docker_demo_docker' terminal=true"
    echo "---- Go | bash='docker_demo_go' terminal=true"
    echo "---- Helm | bash='docker_demo_helm' terminal=true"
    echo "---- Maven | bash='docker_demo_maven' terminal=true"
    echo "---- npm | bash='docker_demo_npm' terminal=true"
    echo "---- NuGet | bash='docker_demo_nuget' terminal=true"
    echo "---- P2 | bash='docker_demo_p2' terminal=true"
    echo "---- PyPI | bash='docker_demo_pypi' terminal=true"
    echo "---- R | bash='docker_demo_r' terminal=true"
    echo "---- Raw | bash='docker_demo_raw' terminal=true"
    echo "---- RubyGems | bash='docker_demo_rubygems' terminal=true"
    echo "---- YUM | bash='docker_demo_yum' terminal=true"
    echo "-- Success Metrics (shell) | bash='docker_demo_success_metrics' terminal=true"
    echo "-- WebHook Site | bash='cd $DC_WEBHOOK_SITE_DIR && docker-compose up' terminal=true"
    echo "-- Node WebHook | bash='cd $NODE_WEBHOOK_DIR && node index.js' terminal=true"
	echo "---"

else
	echo "(p)$BIT_BAR_MENU_TITLE | color=$menubar_paused_color"

	#echo "⚓️ | dropdown=false"
	echo "---"
	echo "Paused (Click to Continue) | bash=$SCRIPTS_DIR/start-docker-poll.sh terminal=false refresh=true"
    echo "Get Info | bash='docker' param1='info' terminal=true"

	exit 0
fi

#############################################################################
# HELPER FUNCTIONS TO SHOW THE MENUS
#############################################################################

function show_inspect_menu() {
	P_CONTAINER_ID=$1

	echo "-- $SYM_LOGS Inspect | bash=$(which docker) param1=inspect param2=$P_CONTAINER_ID terminal=true"
	CONTAINER_INSPECT=`docker inspect $P_CONTAINER_ID`

	echo "---- Networks"
	for i in `echo $CONTAINER_INSPECT | $JQ_BIN -r '.[].NetworkSettings.Networks | keys[]' 2> /dev/null`
	do
		echo "---- $SYM_LOGS $i | color=$cfg_item_color"
	done

	echo "---- IP Address"
	for i in `echo $CONTAINER_INSPECT | $JQ_BIN -r '.[].NetworkSettings.IPAddress' 2> /dev/null`
	do
		echo "---- $SYM_LOGS $i | color=$cfg_item_color"
	done

	echo "---- Volume Binds"
	for i in `echo $CONTAINER_INSPECT | $JQ_BIN -r '.[].HostConfig.Binds[]' 2> /dev/null`
	do
		host_dir=`echo $i | cut -d : -f 1`
		echo "---- $SYM_LOGS $i | bash='cd $host_dir'"

	done

	echo "---- Port Binds"
	for i in `echo $CONTAINER_INSPECT | $JQ_BIN -r '.[].HostConfig.PortBindings[][].HostPort' 2> /dev/null`
	do
		#echo "---- $SYM_LOGS $i"
		echo "---- $SYM_LOGS $i | href=http://host.docker.internal:$i/"
	done

}

function show_my_menu() {
	P_CONTAINER_NAME_ONLY=$1

	if [ "docker-compose_nexus-iq-server_1" == "$P_CONTAINER_NAME_ONLY" ]
	then
		echo "-- Apache HTTPD: | color=$cfg_section_heading_color"
		echo "-- http: $REVERSE_PROXY_PORT/iq | href=http://host.docker.internal:$REVERSE_PROXY_PORT/iq/"
		echo "-- http: $REVERSE_PROXY_PORT/rutiq/rest/support | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutiq/rest/support?includeDb=true&noLimit=true"
		echo "-- http: $REVERSE_PROXY_PORT/rutiq/rest/policy/export | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutiq/rest/policy/organization/ROOT_ORGANIZATION_ID/export"

		echo "-- http: $REVERSE_PROXY_PORT/rutiq | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutiq/assets/index.html"
		echo "-- http: $REVERSE_PROXY_PORT/firewall | href=http://host.docker.internal:$REVERSE_PROXY_PORT/firewall/"

		# H2
		echo "-- H2 (TBD) (stops IQ): | color=$cfg_section_heading_color"
		echo "---- h2 shell - ods | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='ods'"
		echo "---- h2 web console - ods | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='ods' param4='TRUE'"
		echo "---- h2 web console - dm | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='dm' param4='TRUE'"
		echo "---- h2 web console - aggregation | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='aggregation' param4='TRUE'"

		#IQ Links
		echo "-- IQ Admin: | color=$cfg_section_heading_color"
		echo "---- http: 8061/healthcheck | href=http://host.docker.internal:8061/healthcheck"
		echo "---- http: 8061/metrics | href=http://host.docker.internal:8061/metrics?pretty=true"
		#echo "---- http: 8061/tasks/backupDb | href=http://host.docker.internal:8061/tasks/backupDb" #TODO: make a POST
		#echo "---- http: 8061/tasks/triggerPolicyMonitor | href=http://host.docker.internal:8061/tasks/triggerPolicyMonitor"
		echo "---- http: 8061/threads | href=http://host.docker.internal:8061/threads"
		echo "---- http: 8060/support (needs auth) | href=http://host.docker.internal:8060/iq/rest/support?includeDb=true&noLimit=true"
		echo "---- Tasks: | color=$cfg_section_heading_color"
	    echo "------ 8061/tasks/backupDb | bash='curl --request POST http://host.docker.internal:8061/tasks/backupDb' terminal=true"
	    echo "------ 8061/tasks/triggerPolicyMonitor | bash='curl --request POST http://host.docker.internal:8061/tasks/triggerPolicyMonitor' terminal=true"
	    echo "------ 8061/tasks/log-level | bash='curl --request POST http://host.docker.internal:8061/tasks/log-level' terminal=true"
	    echo "------ 8061/tasks/gc | bash='curl --request POST http://host.docker.internal:8061/tasks/gc' terminal=true"
	    echo "------ 8061/tasks/clearReleaseGraphCache | bash='curl --request POST http://host.docker.internal:8061/tasks/clearReleaseGraphCache' terminal=true"
	    echo "------ 8061/tasks/purgeObsoleteReports | bash='curl --request POST http://host.docker.internal:8061/tasks/purgeObsoleteReports' terminal=true"

		#Firewall Links
		echo "-- Firewall (Premium Supported): | color=$cfg_section_heading_color"
		echo "---- go-proxy | href=$IQ_FIREWALL_URL/6b16768f1bfb4050873d6f01b81d2481/result"
		echo "---- maven-central | href=$IQ_FIREWALL_URL/bd22030615004ef79dbf7881661f9f48/result"
		echo "---- npmjs.org-proxy | href=$IQ_FIREWALL_URL/e3302e16eb4d4a749f3369b385d32571/result"
		echo "---- nuget.org-proxy | href=$IQ_FIREWALL_URL/600a2b890de54242982082a2d02d4178/result"
		echo "---- pypy-python.org-proxy | href=$IQ_FIREWALL_URL/0c7c45f4f3c14d8c95f61eacd1de35bb/result"
		echo "---- rubygems.org-releases | href=$IQ_FIREWALL_URL/4792fb7fd8a242eb8583befc298b9ce8/result"
		echo "---- yum-epel | href=$IQ_FIREWALL_URL/7b706cc05b4243579decb87ebf6a4d2d/result"

		echo "-- Firewall (LQA Supported In-Product): | color=$cfg_section_heading_color"
		echo "---- apt | href=$IQ_FIREWALL_URL/5f396b8de8f2429f93cf8adee5d172a3/result"
		echo "---- bower | href=$IQ_FIREWALL_URL/6f703a9b41b5435582254cc16174940d/result"
		echo "---- cocoapods | href=$IQ_FIREWALL_URL/9bc1840ef06a49de98ba970f0a3cdbc4/result"
		echo "---- conan | href=$IQ_FIREWALL_URL/808ff6d7e91a4b838ed433cd30ee5898/result"
		echo "---- conda | href=$IQ_FIREWALL_URL/bb08e6466a114f9284287ea1e36ad750/result"
		echo "---- r | href=$IQ_FIREWALL_URL/077806e669144b4dbee5036b3e736f26/result"

		echo "-- Firewall (LQA Supported Community): | color=$cfg_section_heading_color"
		echo "---- apk | href=$IQ_FIREWALL_URL/c653f30f04e84406adff7f8173f50a07/result"
		echo "---- cargo (hosted repos only) | href=$IQ_FIREWALL_URL/TODO/result"
		echo "---- composer | href=$IQ_FIREWALL_URL/365f0d81a7a6416d90b6c8919244c3f9/result"

		echo "-- Firewall (Un-Supported In-Product): | color=$cfg_section_heading_color"
		echo "---- docker | href=$IQ_FIREWALL_URL/a8afa742625f458488095ad218d77c2b/result"
		echo "---- gitlfs | href=$IQ_FIREWALL_URL/TODO/result"
		echo "---- helm | href=$IQ_FIREWALL_URL/TODO/result"
		echo "---- p2 (mars) | href=$IQ_FIREWALL_URL/885bf0434e1042739a157d2452a3b277/result"
		echo "---- raw-google-android-sdk | href=$IQ_FIREWALL_URL/5fd9cb07257e40a5973df098aa2c22dd/result"
		echo "---- raw-anaconda | href=$IQ_FIREWALL_URL/daab80d0de714c29b5f239be55a6d545/result"
		echo "---- raw-node-saas | href=$IQ_FIREWALL_URL/d8783c9345494a22af4f328ac92d0107/result"

		echo "-- Firewall (Un-Supported Community): | color=$cfg_section_heading_color"
		echo "---- cabal | href=$IQ_FIREWALL_URL/704951eff82949489fbadb8b402a43b7/result"
		echo "---- chef | href=$IQ_FIREWALL_URL/0adba0c4e44e45d0b9904ba2ef196a8f/result"
		echo "---- cpan | href=$IQ_FIREWALL_URL/2eed9bdc7195465ca8683bc8521246a1/result"
		echo "---- elpa | href=$IQ_FIREWALL_URL/80f9f769185b46c7a1d38c4bb1eb10df/result"
		echo "---- ms-symbol-server | href=$IQ_FIREWALL_URL/3c3ed3094dc441f08d6a11cc7635cd23/result"
		echo "---- puppet | href=$IQ_FIREWALL_URL/552a9b8140b346e3a555978565703ad2/result"
		echo "---- terraform (empty repo in github) | href=$IQ_FIREWALL_URL/TODO/result"
		echo "---- labrat | href=$IQ_FIREWALL_URL/a949c47cec8a42aba2bb5fc3ecf733a3/result"

		#IQ UI
		echo "-- IQ Server: | color=$cfg_section_heading_color"

		# Logs
        log_dir=/Users/mworthington/software/docker/nexus-iq-server/var/log/nexus-iq-server

		echo "-- $SYM_LOGS Log Dir | bash='cd $log_dir && ls -laGi *.log'"
		echo "---- request.log | bash='cd $log_dir && tail -F request.log'"
		echo "---- audit.log | bash='cd $log_dir && tail -F audit.log'"
		echo "---- policy-violation.log | bash='cd $log_dir && tail -F policy-violation.log'"
		echo "---- clm-server.log | bash='cd $log_dir && tail -F clm-server.log'"

		# Configuration
		echo "-- $SYM_LOGS Conf Dir | bash='cd $DC_DEMO_DIR/iq-server/etc/nexus-iq-server'"

	elif [ "docker-compose_nexus3_1" == "$P_CONTAINER_NAME_ONLY" ] || [ "docker-compose_nexus3-proxy_1" == "$P_CONTAINER_NAME_ONLY" ]
	then
		echo "-- Apache HTTPD: | color=$cfg_section_heading_color"
		echo "-- http: $REVERSE_PROXY_PORT/nexus | href=http://host.docker.internal:$REVERSE_PROXY_PORT/nexus/"
		echo "-- http: $REVERSE_PROXY_PORT/rutnexus | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/"
		echo "-- http: proxy 8183/nexus | href=http://host.docker.internal:8183/nexus/"

        #Orient
		echo "-- orient: | color=$cfg_section_heading_color"
		echo "---- http: 2480/studio | href=http://host.docker.internal:2480/studio/index.html"

        #Elastic Search
		echo "-- elasticsearch: | color=$cfg_section_heading_color"
		echo "---- http: 9200/kopf | href=http://host.docker.internal:9200/_plugin/kopf"

		#RM Links
		echo "-- RM Admin: | color=$cfg_section_heading_color"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/healthcheck | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/metrics/healthcheck"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/metrics/data | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/metrics/data"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/metrics/prometheus | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/metrics/prometheus"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/status | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/rest/v1/status"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/writable | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/rest/v1/status/writable"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/ping | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/metrics/ping"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/threads | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/service/metrics/threads"
		echo "---- http: $REVERSE_PROXY_PORT/rutnexus/swagger | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus/#admin/system/api"

		# RM Demo & FW Demo
		echo "-- RM Proxy Demos | color=$cfg_section_heading_color"
		echo "---- conda | bash='docker run -it --rm $ALT_DOCKER_REGISTRY/continuumio/miniconda3'"


        #Nexus UI
		echo "-- Nexus3: | color=$cfg_section_heading_color" 

		# Logs
        log_dir=/Users/mworthington/software/docker/nexus3/nexus-data/log

		echo "-- $SYM_LOGS Log Dir | bash='cd $log_dir && ls -laGi *.log'"
		echo "---- audit.log | bash='cd $log_dir/audit && tail -F audit.log'"
		echo "---- request.log | bash='cd $log_dir && tail -F request.log'"
		echo "---- nexus.log | bash='cd $log_dir && tail -F nexus.log'"
		echo "---- outbound.log | bash='cd $log_dir && tail -F outbound.log'"
		echo "---- script.log | bash='cd $log_dir && tail -F script.log'"
		echo "---- nexus_cluster.log | bash='cd $log_dir && tail -F nexus_cluster.log'"

		# Configuration
		echo "-- $SYM_LOGS Conf Dir | bash='cd $DC_DEMO_DIR/nexus3/opt/sonatype/nexus/etc'"

	elif [ "docker-compose_nexus2_1" == "$P_CONTAINER_NAME_ONLY" ]
	then

		echo "-- Apache HTTPD: | color=$cfg_section_heading_color" 
		echo "-- http: $REVERSE_PROXY_PORT/nexus2 | href=http://host.docker.internal:$REVERSE_PROXY_PORT/nexus2/"
		echo "-- http: $REVERSE_PROXY_PORT/rutnexus2 | href=http://host.docker.internal:$REVERSE_PROXY_PORT/rutnexus2/"

		echo "-- H2 (TBD) (stops RM): | color=$cfg_section_heading_color"
		echo "---- h2 shell - nuget/odata | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='nuget/odata'"
		echo "---- h2 web console - nuget/odata | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='nuget/odata' param4='TRUE'"
		echo "---- h2 web console - access/ips | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='access/ips' param4='TRUE'"
		echo "---- h2 web console - db/usertoken | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='db/usertoken' param4='TRUE'"
		echo "---- h2 web console - db/analytics/analytics | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='db/analytics/analytics' param4='TRUE'"

		#RM Links
		echo "-- RM Admin: | color=$cfg_section_heading_color"
		echo "---- http: 8082/metrics | href=http://host.docker.internal:8082/nexus/internal/metrics"

        #Nexus UI
		echo "-- Nexus2: | color=$cfg_section_heading_color" 

		# Logs
        log_dir=/Users/mworthington/software/docker/nexus2/sonatype-work/logs

		echo "-- $SYM_LOGS Log Dir | bash='cd $log_dir && ls -laGi *.log'"
		echo "---- request.log | bash='cd $log_dir && tail -F request.log'"
		echo "---- nexus.log | bash='cd $log_dir && tail -F nexus.log'"

		# Configuration
		echo "-- $SYM_LOGS Conf Dir | bash='cd $DC_DEMO_DIR/nexus2/opt/sonatype/nexus'"

	elif [ "docker-compose_httpd_1" == "$P_CONTAINER_NAME_ONLY" ]
	then
		echo "-- Apache HTTPD: | color=$cfg_section_heading_color"
		echo "---- http: $REVERSE_PROXY_PORT/server-status | href=http://host.docker.internal:$REVERSE_PROXY_PORT/server-status/"
		echo "---- http: $REVERSE_PROXY_PORT/server-info | href=http://host.docker.internal:$REVERSE_PROXY_PORT/server-info/"
		
		echo "---- (todo )balancer-manager docs | href=https://httpd.apache.org/docs/2.4/howto/reverse_proxy.html"
		echo "---- http: $REVERSE_PROXY_PORT/balancer-manager | href=http://host.docker.internal:$REVERSE_PROXY_PORT/balancer-manager/"

        # Logs
        log_dir=/Users/mworthington/software/docker/httpd/usr/local/apache2/logs

		echo "-- $SYM_LOGS Log Dir | bash='cd $log_dir && ls -laGi *.log'"
		echo "---- access_log | bash='cd $log_dir && tail -F access_log'"
		echo "---- error_log | bash='cd $log_dir && tail -F error_log'"

		# Configuration
		echo "-- $SYM_LOGS Conf Dir | bash='cd $DC_DEMO_DIR/httpd/conf'"

	elif [ "docker-compose_dependency-track_1" == "$P_CONTAINER_NAME_ONLY" ]
	then
		echo "-- http: Swagger UI | href=chrome-extension://ljlmonadebogfjabhkppkoohjkjclfai/index.html?url=http://host.docker.internal:8050/api/swagger.json"

	elif [ "docker-compose_jenkins_1" == "$P_CONTAINER_NAME_ONLY" ]
	then
		echo "-- Jenkins Admin: | color=$cfg_section_heading_color"
		echo "---- http: 9080 (env-vars) | href=http://host.docker.internal:9080/env-vars.html/"

        # Logs
        log_dir=/Users/mworthington/software/docker/jenkins/var/jenkins_home/logs

		echo "-- $SYM_LOGS Log Dir | bash='cd $log_dir && ls -laGi *.log'"

		# Configuration
		echo "-- $SYM_LOGS Conf Dir | bash='cd $log_dir/..'"
	fi
}

function show_up_container() {
	P_CONTAINER_NAME_ONLY=$1
	P_CONTAINER_ID=$2
	P_CONTAINER_IMAGE=$3

	echo "$SYM $P_CONTAINER_NAME_ONLY - $P_CONTAINER_IMAGE | color=$cfg_item_running_color"

    show_my_menu $P_CONTAINER_NAME_ONLY
	show_inspect_menu $P_CONTAINER_ID
	echo "-- $SYM_LOGS Shell | bash=$(which docker) param1=exec param2=-it param3=$P_CONTAINER_NAME_ONLY param4=bash terminal=true"
	echo "-- $SYM_LOGS Shell (root) | bash=$(which docker) param1=exec param2=-u param3=0 param4=-it param5=$P_CONTAINER_ID param6=bash terminal=true"
	echo "-- $SYM_LOGS Logs | bash=$(which docker) param1=logs param2=-f param3=$P_CONTAINER_ID terminal=true"
	echo "-- $SYM_END Stop | color=$cfg_item_action_stop_color bash=$(which docker) param1=stop param2=$P_CONTAINER_ID terminal=false refresh=true"
	echo "-- $SYM_END Restart | color=$cfg_item_action_stop_color bash=$(which docker) param1=restart param2=$P_CONTAINER_ID terminal=false refresh=true"

  #TODO: Pull the ports from the docker port binding
	if [ $IS_RM -eq 0 ]
	then
		i=8081
		echo "$SYM_LOGS port (TODO: default): $i | href=http://host.docker.internal:$i/"
	fi

	if [ $IS_IQ -eq 0 ]
	then
		i=8070
		echo "$SYM_LOGS port (TODO: default): $i | href=http://host.docker.internal:$i/"
	fi

	if [ $IS_JENKINS -eq 0 ]
	then
		i=8080
		echo "$SYM_LOGS port (TODO: default): $i | href=http://host.docker.internal:$i/"
	fi
}

function show_exited_container() {
	P_CONTAINER_NAME_ONLY=$1
	P_CONTAINER_ID=$2
	P_CONTAINER_IMAGE=$3

	echo "$SYM $P_CONTAINER_NAME_ONLY - $P_CONTAINER_IMAGE | color=$cfg_item_stopped_color"
    show_my_menu $P_CONTAINER_NAME_ONLY
	show_inspect_menu $P_CONTAINER_ID
	echo "-- $SYM_LOGS Remove | bash=$(which docker) param1=rm param2=$P_CONTAINER_ID terminal=false refresh=true"
	echo "-- $SYM_LOGS Shell | bash=$(which docker) param1=run param2=-it param3=--rm param4=$P_CONTAINER_IMAGE param5=bash terminal=true"
	echo "-- $SYM_LOGS Logs | bash=$(which docker) param1=logs param2=-f param3=$P_CONTAINER_ID terminal=true"
	echo "-- $SYM_END Start | color=$cfg_item_action_start_color bash=$(which docker) param1=start param2=$P_CONTAINER_ID terminal=false refresh=true"
}

function show_other_container() {
	P_CONTAINER_NAME_ONLY=$1
	P_CONTAINER_ID=$2
	P_CONTAINER_IMAGE=$3
	P_CONTAINER_STATE=$4

	echo "$SYM $P_CONTAINER_NAME_ONLY - $P_CONTAINER_STATE - $P_CONTAINER_IMAGE | color=$cfg_item_action_stop_color"
    show_my_menu $P_CONTAINER_NAME_ONLY
	show_inspect_menu $P_CONTAINER_ID
	echo "-- $SYM_LOGS Remove | bash=$(which docker) param1=rm param2=$P_CONTAINER_ID terminal=false refresh=true"

}

#############################################################################
# TOP LEVEL MENU
#############################################################################
function containers() {
	P_STATUS_TO_SHOW=$1
	P_EXITED_FILTER=$2
	FOUND_UP_CONTAINER="FALSE"
	FOUND_EXITED_CONTAINER="FALSE"
	FOUND_EXITED_NEXUS_HA_TEST_CONTAINER="FALSE"
	FOUND_EXITED_CS_AUTH_PROXY_CONTAINER="FALSE"
	FOUND_EXITED_LOCAL_MIKE_CONTAINER="FALSE"
	FOUND_OTHER_CONTAINER="FALSE"

	#TODO: optimize using exited and running counts as in the header

	#CONTAINERS="$(docker ps -a --format "{{.Names}} ({{.Image}}) ({{.Ports}})|{{.ID}}|{{.Status}}" | sort)"
	# -a = all
	# --no-trunc
	# --size
	#
	# CONTAINER ID                      4.
	# IMAGE                             2.                                       
	# COMMAND                                                                                                                                    
	# CREATED            
	# STATUS                              5.
	# PORTS                               3.                                                  
	# NAMES                               1.
	# SIZE

	CONTAINERS="$(docker ps -a --format "{{.Names}}|{{.Image}}|{{.Ports}}|{{.ID}}|{{.Status}}" | sort)"
	
	if [ -z "$CONTAINERS" ]; 
	then
		echo "No containers"
	else
		LAST_CONTAINER=$(echo "$CONTAINERS" | tail -n1 )
		#echo "${CONTAINERS}" | while read -r CONTAINER; 
		while read -r CONTAINER
		do
			CONTAINER_NAME_ONLY=$(echo "$CONTAINER" | awk -F"|" '{print $1}')
			CONTAINER_IMAGE=$(echo "$CONTAINER" | awk -F"|" '{print $2}')
			CONTAINER_PORTS=$(echo "$CONTAINER" | awk -F"|" '{print $3}')
			CONTAINER_ID=$(echo "$CONTAINER" | awk -F"|" '{print $4}')
			CONTAINER_STATE=$(echo "$CONTAINER" | awk -F"|" '{print $5}')

			CONTAINER_NAME="$CONTAINER_NAME_ONLY ($CONTAINER_IMAGE) ($CONTAINER_PORTS)"
			SYM="├ 💻 "
			SYM_LOGS="├       "
			SYM_END="├       "

			if [ "$CONTAINER" = "$LAST_CONTAINER" ]; 
			then 
#				SYM="└ 💻 ";
				SYM="├ 💻 "
				SYM_LOGS="├       "
				SYM_END="└       "
			fi

			#is k8s?
			echo $CONTAINER_NAME | grep -i "^k8s_" >/dev/null 2>&1
			IS_K8S=$?

			#is RM?
			echo $CONTAINER_NAME | grep -i "nexus-persistent-store" >/dev/null 2>&1
			IS_RM=$?
			
			#is IQ?
			echo $CONTAINER_NAME | grep -i "iq-server-persistent-store" >/dev/null 2>&1
			IS_IQ=$?

			#is Jenkins?
			echo $CONTAINER_NAME | grep -i "jenkins-persistent-store" >/dev/null 2>&1
			IS_JENKINS=$?

			
			case "$CONTAINER_STATE" in
				*Up*) 
						if [ "$P_STATUS_TO_SHOW" = "Up" ] #TODO: this could be optimized, but good enough for now
						then
							FOUND_UP_CONTAINER="TRUE"

							case "$P_EXITED_FILTER" in
								"k8s")
									if [ $IS_K8S -eq 0 ]
									then
										show_up_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								"all-others")
									if [ $IS_K8S -eq 1 ]
									then
										show_up_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								*)
									echo "ERROR: Unexpected Exited Filter: $P_EXITED_FILTER"
									;;
							esac
						fi;;
				*Exited*) 
						if [ "$P_STATUS_TO_SHOW" = "Exited" ]
						then
							echo $CONTAINER_IMAGE | grep -i "nexus-ha-test" >/dev/null 2>&1
							IS_NEXUS_HA_TEST=$?

							echo $CONTAINER_NAME_ONLY | grep -i "^cs-auth-proxy_" >/dev/null 2>&1
							IS_CS_AUTH_PROXY=$?

							#echo $CONTAINER_IMAGE | grep -i "^local-mike:" >/dev/null 2>&1
							echo $CONTAINER_IMAGE | grep -i "^host.docker.internal:" >/dev/null 2>&1
							IS_LOCAL_MIKE=$?

							case "$P_EXITED_FILTER" in
								"nexus-ha-test")
									if [ $IS_NEXUS_HA_TEST -eq 0 ]
									then
										FOUND_EXITED_NEXUS_HA_TEST_CONTAINER="TRUE"

										show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								"cs-auth-proxy")
									if [ $IS_CS_AUTH_PROXY -eq 0 ]
									then
										FOUND_EXITED_CS_AUTH_PROXY_CONTAINER="TRUE"

										show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								"local-mike")
									if [ $IS_LOCAL_MIKE -eq 0 ]
									then
										FOUND_EXITED_LOCAL_MIKE_CONTAINER="TRUE"

										show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								"all-others")
									if [ $IS_NEXUS_HA_TEST -eq 1 ] && [ $IS_CS_AUTH_PROXY -eq 1 ] && [ $IS_LOCAL_MIKE -eq 1 ]
									then
										FOUND_EXITED_CONTAINER="TRUE"

										show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
									fi
									;;
								*)
									echo "ERROR: Unexpected Exited Filter: $P_EXITED_FILTER"
									;;
							esac

							# if [ "$P_EXITED_FILTER" = "nexus-ha-test" ]
							# then
							# 	if [ $IS_NEXUS_HA_TEST -eq 0 ]
							# 	then
							# 		FOUND_EXITED_NEXUS_HA_TEST_CONTAINER="TRUE"
							# 		show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
							# 	fi
							# fi
							# if [ "$P_EXITED_FILTER" = "all-others" ]
							# then
							# 	if [ $IS_NEXUS_HA_TEST -eq 1 ]
							# 	then
							# 		FOUND_EXITED_CONTAINER="TRUE"
							# 		show_exited_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE
							# 	fi
							# fi
						fi;;
				*)
						if [ "$P_STATUS_TO_SHOW" = "Other" ]
						then
							FOUND_OTHER_CONTAINER="TRUE"

							show_other_container $CONTAINER_NAME_ONLY $CONTAINER_ID $CONTAINER_IMAGE $CONTAINER_STATE
						fi;;
			esac
		done <<< "${CONTAINERS}" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered


		SYM_END="└       "

		#echo "Status to Show: $P_STATUS_TO_SHOW"
		#echo "Found Up: $FOUND_UP_CONTAINER"
		#echo "Found Exit: $FOUND_EXITED_CONTAINER"

		if [ "$P_STATUS_TO_SHOW" = "Up" ] && [ "$FOUND_UP_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Running Containers"
		fi

		if [ "$P_STATUS_TO_SHOW" = "Exited" ] && [ "$P_EXITED_FILTER" = "all-others" ] && [ "$FOUND_EXITED_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Stopped Containers"
		fi

		if [ "$P_STATUS_TO_SHOW" = "Exited" ] && [ "$P_EXITED_FILTER" = "nexus-ha-test" ] && [ "$FOUND_EXITED_NEXUS_HA_TEST_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Stopped Nexus HA Test Containers"
		fi

		if [ "$P_STATUS_TO_SHOW" = "Exited" ] && [ "$P_EXITED_FILTER" = "cs-auth-proxy" ] && [ "$FOUND_EXITED_CS_AUTH_PROXY_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Stopped Nexus HA Test Containers"
		fi

		if [ "$P_STATUS_TO_SHOW" = "Exited" ] && [ "$P_EXITED_FILTER" = "local-mike" ] && [ "$FOUND_EXITED_LOCAL_MIKE_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Stopped Nexus HA Test Containers"
		fi

		if [ "$P_STATUS_TO_SHOW" = "Other" ] && [ "$FOUND_OTHER_CONTAINER" = "FALSE" ]
		then
			echo "$SYM_END No Other Containers"
		fi

	fi
}

function images() {
	IMAGES="$(docker images --format "{{.Repository}}|{{.Tag}}|{{.ID}}" | sort)"
	
	if [ -z "$IMAGES" ]; 
	then
		echo "No images"
	else
		echo "$SYM Image List | bash=$(which docker) param1=images terminal=true"

		LAST_IMAGE=$(echo "$IMAGES" | tail -n1 )
		#echo "${IMAGES}" | while read -r IMAGE; 
		while read -r IMAGE
		do
			IMAGE_NAME=$(echo "$IMAGE" | awk -F"|" '{print $1}')
			IMAGE_TAG=$(echo "$IMAGE" | awk -F"|" '{print $2}')
			IMAGE_ID=$(echo "$IMAGE" | awk -F"|" '{print $3}')
			SYM="├ 💻 "
			SYM_LOGS="├       "
			SYM_END="├       "

			if [ "$IMAGE" = "$LAST_IMAGE" ]; 
			then 
#				SYM="└ 💻 ";
				SYM="├ 💻 "
				SYM_LOGS="├       "
				SYM_END="└       "
			fi
			
			echo "-- $SYM $IMAGE_NAME ($IMAGE_TAG)"
			# you want to remove by name and tag in case the same image was pulled from multiple sources then it would have the same id, and therefore remove all if done by id
			echo "---- $SYM_END Remove | bash=$(which docker) param1=rmi param2=$IMAGE_NAME:$IMAGE_TAG terminal=true refresh=true"
			#echo "-- $SYM_END Force Remove | bash=$(which docker) param1=rmi param2=-f param3=$IMAGE_NAME:$IMAGE_TAG terminal=true refresh=true"
			#echo "-- $SYM_END Remove | bash=$(which docker) param1=rmi param2=$IMAGE_ID terminal=true refresh=true"
		done <<< "${IMAGES}" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered
	fi
}

function volumes() {
	VOLUMES="$(docker volume ls --format "{{.Name}}|{{.Driver}}|{{.Mountpoint}}" | sort)"
	
	if [ -z "$VOLUMES" ]; 
	then
		echo "No volumes"
	else
		echo "$SYM Volume List | bash=$(which docker) param1=volume param2=ls terminal=true"

		# exit early
		# I have a lot of volumes, and this does not show useful information at the moment
		return 0

		LAST_VOLUME=$(echo "$VOLUMES" | tail -n1 )
		#echo "${VOLUMES}" | while read -r VOLUME; 
		while read -r VOLUME
		do
			VOLUME_NAME=$(echo "$VOLUME" | awk -F"|" '{print $1}')
			VOLUME_DRIVER=$(echo "$VOLUME" | awk -F"|" '{print $2}')
			VOLUME_MOUNTPOINT=$(echo "$VOLUME" | awk -F"|" '{print $3}')
			SYM="├ 💻 "
			SYM_LOGS="├       "
			SYM_END="├       "

			if [ "$VOLUME" = "$LAST_VOLUME" ]; 
			then 
#				SYM="└ 💻 ";
				SYM="├ 💻 "
				SYM_LOGS="├       "
				SYM_END="└       "
			fi
			
			echo "-- $SYM $VOLUME_NAME"

			echo "---- $SYM_LOGS Inspect | bash=$(which docker) param1=volume param2=inspect param3=$VOLUME_NAME terminal=true"
			VOLUME_INSPECT=`docker volume inspect $VOLUME_NAME`
			VOLUME_CREATED_AT=`echo $VOLUME_INSPECT | $JQ_BIN -r '.[].CreatedAt' 2> /dev/null`
			echo "------ Mountpoint: $VOLUME_MOUNTPOINT | color=$cfg_item_color"
			echo "------ Created At: $VOLUME_CREATED_AT | color=$cfg_item_color"

			echo "---- $SYM_END Remove | bash=$(which docker) param1=volume param2=rm param3=$VOLUME_NAME terminal=true refresh=true"
			#echo "-- $SYM_END Force Remove | bash=$(which docker) param1=rmi param2=-f param3=$VOLUME_NAME:$VOLUME_TAG terminal=true refresh=true"
			#echo "-- $SYM_END Remove | bash=$(which docker) param1=rmi param2=$VOLUME_ID terminal=true refresh=true"
		done <<< "${VOLUMES}" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered
	fi
}

function networks() {
    NETWORKS="$(docker network ls --format "{{.Name}}|{{.Driver}}|{{.Scope}}|{{.ID}}" | sort)" 
	
	if [ -z "$NETWORKS" ]; 
	then
		echo "No networks"
	else
		echo "$SYM Network List | bash=$(which docker) param1=network param2=ls terminal=true"

		LAST_NETWORK=$(echo "$NETWORKS" | tail -n1 )
		#echo "${NETWORKS}" | while read -r NETWORK; 
		while read -r NETWORK
		do
			NETWORK_NAME=$(echo "$NETWORK" | awk -F"|" '{print $1}')
			NETWORK_DRIVER=$(echo "$NETWORK" | awk -F"|" '{print $2}')
			SYM="├ 💻 "
			SYM_LOGS="├       "
			SYM_END="├       "

			if [ "$NETWORK" = "$LAST_NETWORK" ]; 
			then 
#				SYM="└ 💻 ";
				SYM="├ 💻 "
				SYM_LOGS="├       "
				SYM_END="└       "
			fi
			
			echo "-- $SYM $NETWORK_NAME"

			echo "---- $SYM_LOGS Inspect | bash=$(which docker) param1=network param2=inspect param3=$NETWORK_NAME terminal=true"
			NETWORK_INSPECT=`docker network inspect $NETWORK_NAME`
			NETWORK_CREATED_AT=`echo $NETWORK_INSPECT | $JQ_BIN -r '.[].Created' 2> /dev/null`
			NETWORK_SUBNET=`echo $NETWORK_INSPECT | $JQ_BIN -r '.[].IPAM.Config[].Subnet' 2> /dev/null`
			echo "------ Created: $NETWORK_CREATED_AT | color=$cfg_item_color"
			echo "------ Subnet: $NETWORK_SUBNET | color=$cfg_item_color"

			echo "---- $SYM_END Remove | bash=$(which docker) param1=network param2=rm param3=$NETWORK_NAME terminal=true refresh=true"
		done <<< "${NETWORKS}" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered
	fi
}

###############################################################
#CONTAINERS
###############################################################
echo "Docker Running Containers"
containers Up all-others

echo "---"

echo "K8s Running Containers"
containers Up k8s

echo "---"

echo "Docker Stopped Containers"
containers Exited all-others

echo "Docker Stopped NXRM HA Containers"
containers Exited nexus-ha-test

echo "Docker Stopped CS Auth Proxy Containers"
containers Exited cs-auth-proxy

echo "Docker Stopped Local Mike Containers"
containers Exited local-mike

echo "Docker Other Containers"
containers Other

echo "---"

###############################################################
#IMAGES
###############################################################
IMAGES="$(docker images --format "{{.Repository}} ({{.Tag}})|{{.ID}}")"  #TODO: reformat and pass as a param to the function
#echo "IMAGES = $IMAGES"

if [ -n "$IMAGES" ]; 
then
	echo "Docker Images"
	images
fi

###############################################################
#VOLUMES
###############################################################
VOLUMES="$(docker volume ls --format "{{.Name}}|{{.Driver}}|{{.Mountpoint}}")"  #TODO: reformat and pass as a param to the function
#echo "VOLUMES = $VOLUMES"

if [ -n "$VOLUMES" ]; 
then
	echo "Docker Volumes"
	volumes
fi

###############################################################
#NETWORKS
###############################################################
NETWORKS="$(docker network ls --format "{{.Name}}|{{.Driver}}|{{.Scope}}|{{.ID}}")"  #TODO: reformat and pass as a param to the function
#echo "NETWORKS = $NETWORKS"

if [ -n "$NETWORKS" ]; 
then
	echo "Docker Networks"
	networks
fi
