#!/bin/bash
source /Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/config.cfg 

#BIT_BAR_MENU_TITLE="Nexus 3"
BIT_BAR_MENU_TITLE="NX3"

function find_ports_in_config_file ()
{
	local p_config_file=$1
	local rc=1
	local context="/"

	#flag to identify if we found a context but not a port
	local context_with_no_port="11"

	#for my apache demo
	# more colors : yellow, cadetblue
	if [ "/Users/mworthington/Downloads/nexus/nexus3-demo/current/../sonatype-work/nexus3/etc/nexus.properties" == "$p_config_file" ] \
		|| [ "/Users/mworthington/Downloads/nexus/nexus3-demo-node1/current/../sonatype-work/nexus3/etc/nexus.properties" == "$p_config_file" ]
	then
		echo "Apache HTTPD: | color=#770000"
		echo "-- http: 80/server-status | href=http://127.0.0.1/server-status/"
		echo "-- http: 80/server-info | href=http://127.0.0.1/server-info/"
		echo "http: 80/nexus | href=http://localhost/nexus/"
		echo "http: 80/rutnexus | href=http://localhost/rutnexus/"

		echo "Log Dir | bash='cd /var/log/apache2/'"
		echo "-- access_log | bash='cd /var/log/apache2/ && tail -F access_log'"
		echo "-- error_log | bash='cd /var/log/apache2/ && tail -F error_log'"
		echo "Conf Dir | bash='cd /etc/apache2/'"

        #Orient
		echo "orient: | color=#770000"
		echo "-- http: 2480/studio | href=http://localhost:2480/studio/index.html"

        #Elastic Search
		echo "elasticsearch: | color=#770000"
		echo "-- http: 9200/kopf | href=http://localhost:9200/_plugin/kopf"

        #Hazelcast
		echo "hazelcast: | color=#770000"
		echo "-- http: 8080/ | href=http://localhost:8080/"

		#RM Links
		echo "RM Admin: | color=#770000"
		echo "-- http: 80/rutnexus/healthcheck | href=http://localhost/rutnexus/service/metrics/healthcheck"
		echo "-- http: 80/rutnexus/metrics/data | href=http://localhost/rutnexus/service/metrics/data"
		echo "-- http: 80/rutnexus/metrics/prometheus | href=http://localhost/rutnexus/service/metrics/prometheus"
		echo "-- http: 80/rutnexus/ping | href=http://localhost/rutnexus/service/metrics/ping"
		echo "-- http: 80/rutnexus/threads | href=http://localhost/rutnexus/service/metrics/threads"
		echo "-- http: 80/rutnexus/swagger | href=http://localhost/rutnexus/#admin/system/api"

        #Nexus UI
		echo "nexus: | color=#770000" 
	fi

	for i in `grep nexus-context-path= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one
		context_with_no_port="01"

	    context="$i"
	done

	#PORT="$(grep application-port $nexus3_dir/etc/org.sonatype.nexus.cfg)"
	#port_number=`echo $PORT | cut -f 2- -d = | tr -d '[[:space:]]'`
	#echo "${PORT} | href=http://localhost:${port_number}/"

	for i in `grep application-port= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one
		context_with_no_port="00"

		if [ "8081" = "$i" ]
		then
		    echo "http (default): $i$context | href=http://localhost:$i$context"
		else

		    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
		    #echo "port: $i | bash='open http://localhost:$i && exit'"
		    echo "http: $i$context | href=http://localhost:$i$context"
		fi
	done

	for i in `grep application-port-ssl= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one
		context_with_no_port="00"

	    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
	    #echo "port: $i | bash='open http://localhost:$i && exit'"
	    echo "https: $i$context | href=https://localhost:$i$context"
	done

	if [ "01" = "$context_with_no_port" ]
	then
		echo "Found a context configured, but not a port. The logic is too complex for me now."
		echo "Please specify both in the same file: $p_config_file"
	fi

	return $rc
}

#ps aux | grep nexus-professional-2 | grep -v grep >/dev/null 2>&1`
#instances=`ps aux | grep nexus-professional-2 | grep -v grep | wc -l | tr -d '[[:space:]]'`
#instances=`ps aux | grep wrapper.conf | grep -v grep | wc -l | tr -d '[[:space:]]'`

ps_cmd="ps aux | grep org.sonatype.nexus.karaf.NexusMain | grep -v grep | sort"

instances=`eval $ps_cmd | wc -l | tr -d '[[:space:]]'`
[ $instances -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${BIT_BAR_MENU_TITLE}($instances) | color=$menubar_running_color"

#http://www.staff.science.uu.nl/~oostr102/docs/nawk/nawk_92.html
#nexus 3.0 only 
#instance_dirs=`ps aux | grep org.sonatype.nexus.karaf.NexusMain | grep -v grep | awk '{str=$11; sub("\.install4j\/jre\.bundle\/Contents\/Home\/jre\/bin\/java", "", str); print str}' | sort`

#hack for now - ideally i  think i'd implement the bitbar-pwd -D param structure the same way i have for IQ Server
#instance_dirs=`ps aux | grep org.sonatype.nexus.karaf.NexusMain | grep -v grep | awk '{str=$14; sub("-Dexe4j.moduleName=", "", str); sub("/bin/nexus", "", str); print str " | "}' | sort`

instance_dirs=""

while read line
do
	# the while loop executes for some reason when line is zero (i.e. if there are no processes running)
    if [ -n "$line" ] 
    then
		echo "---"

	    instance_pid=`echo $line | awk '{print $2}'`

	    echo $line | grep "bitbar-pwd" > /dev/null 2>&1
	    has_bitbar=$?

	    instance_dir=""
		if [ $has_bitbar -eq 0 ]
	    then
		    #finding the field that matches a string = http://www.tek-tips.com/viewthread.cfm?qid=1358154
		    instance_dir=`echo $line | awk '{for(i=1;i<=NF;++i)if($i~/bitbar-pwd/)str=$i; sub("-Dbitbar-pwd=", "", str); print str}'` #nexus.vmoptions : -Dbitbar-pwd=/Users/mworthington/Downloads/nexus/nexus3-demo/current
		else
			echo "bitbar not configured in nexus.vmoptions. falling back to ps aux"
			#instance_dir=`ps aux | grep $instance_pid | grep -v grep | awk '{str=$14; sub("-Dexe4j.moduleName=", "", str); sub("/bin/nexus", "", str); print str }'`
			instance_dir=`echo $line | awk '{str=$14; sub("-Dexe4j.moduleName=", "", str); sub("/bin/nexus", "", str); print str }'`
		fi

		if [ -n "$instance_dir" ]
	    then
		    instance_dirs="$instance_dirs $instance_dir |"

			echo "$instance_dir ($instance_pid) | color=green"

			nexus30_cfg_file="$instance_dir/etc/org.sonatype.nexus.cfg"
			nexus31_cfg_file_default="$instance_dir/etc/nexus-default.properties"
			nexus31_cfg_file_user="$instance_dir/../sonatype-work/nexus3/etc/nexus.properties"

			# if a nexus 3.0 instance (org.sonatype.nexus.cfg)
			if [ -r "$nexus30_cfg_file" ]
			then
				find_ports_in_config_file "$nexus30_cfg_file"

				#echo "---"
				echo "Log Dir | bash='cd $instance_dir/data/log'"
				echo "-- request.log | bash='cd $instance_dir/data/log && tail -F request.log'"
				echo "-- nexus.log | bash='cd $instance_dir/data/log && tail -F nexus.log'"
				#echo "---"
			else
				# a nexus 3.1+ instance (nexus.properties)
				find_ports_in_config_file "$nexus31_cfg_file_user"

				if [ 0 -ne $? ]
				then
					echo "(using nexus-default.properties ports)"
					find_ports_in_config_file "$nexus31_cfg_file_default"			
				fi

				#echo "---"
				echo "Log Dir | bash='cd $instance_dir/../sonatype-work/nexus3/log'"
				echo "-- audit.log | bash='cd $instance_dir/../sonatype-work/nexus3/log/audit && tail -F audit.log'"
				echo "-- request.log | bash='cd $instance_dir/../sonatype-work/nexus3/log && tail -F request.log'"
				echo "-- nexus.log | bash='cd $instance_dir/../sonatype-work/nexus3/log && tail -F nexus.log'"
				echo "-- nexus_cluster.log | bash='cd $instance_dir/../sonatype-work/nexus3/log && tail -F nexus_cluster.log'"
				#echo "---"
			fi

			echo "Stop Nexus 3 | color=purple bash='/bin/bash' param1='-c' param2='$instance_dir/bin/nexus stop' terminal=false refresh=true"
			echo "Restart Nexus 3 | color=purple bash='/bin/bash' param1='-c' param2='$instance_dir/bin/nexus stop && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/bin/nexus start' terminal=false refresh=true"
		else
			echo "No Dir for PID: $instance_pid"
		fi
	fi
done <<< "$(eval $ps_cmd)" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered

echo "---"
#echo "instance_dirs: $instance_dirs"
#echo "---"

for (( i=0; i < ${#nexus3_dirs[@]}; i++ ))
do
	#this_base_dir=`dirname ${nexus3_dirs[$i]}`
	this_base_dir=${nexus3_dirs[$i]}

#	echo "this_base_dir: $this_base_dir"
	echo $instance_dirs | grep "$this_base_dir |" > /dev/null 2>&1
	if [ $? -gt 0 ]
	then
		#bitbar colors can use friendly name or hex code
		#https://getbitbar.com/plugins/Tutorial/ansi.sh
		echo "Start Nexus 3 (${this_base_dir}) | color=#123def bash='/bin/bash' param1='-c' param2='${this_base_dir}/bin/nexus start' terminal=false refresh=true"
		#echo "Start Nexus 3 (${this_base_dir}) | color=blue bash='${this_base_dir}/bin/nexus start && exit' terminal=true"
	fi
done
