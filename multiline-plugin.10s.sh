#!/bin/bash
source /Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/config.cfg 

#BIT_BAR_MENU_TITLE="Nexus 2"
BIT_BAR_MENU_TITLE="NX2"

function find_ports_in_config_file ()
{
	local p_config_file=$1
	local rc=1
	local context="/nexus"

	#for my apache demo
	# more colors : yellow, cadetblue
	rm_demo_dir="/Users/mworthington/Downloads/nexus/nexus-iq-demo"
	if [ "$rm_demo_dir/current/conf/nexus.properties" == "$p_config_file" ]
	then
		echo "apache: | color=orange" 
		echo "http: 80/server-status | href=http://localhost/server-status/"
		echo "http: 80/server-info | href=http://localhost/server-info/"
		echo "http: 80/nexus2 | href=http://localhost/nexus2/"
		echo "http: 80/rutnexus2 | href=http://localhost/rutnexus2/"
		echo "Log Dir | bash='cd /var/log/apache2/'"
		echo "-- access_log | bash='cd /var/log/apache2/ && tail -F access_log'"
		echo "Conf Dir | bash='cd /etc/apache2/'"
		echo "h2 (stops RM): | color=orange"
		echo "h2 shell - nuget/odata | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='nuget/odata'"
		echo "h2 web console - nuget/odata | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='nuget/odata' param4='TRUE'"
		echo "h2 web console - access/ips | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='access/ips' param4='TRUE'"
		echo "h2 web console - db/usertoken | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='db/usertoken' param4='TRUE'"
		echo "h2 web console - db/analytics/analytics | terminal=true bash='$rm_demo_dir/h2-shell-odata.sh' param1='$rm_demo_dir' param2='$instance_pid' param3='db/analytics/analytics' param4='TRUE'"
		echo "nexus: | color=orange" 
		echo "http: 8082/metrics | href=http://localhost:8082/nexus/internal/metrics"
	fi

	for i in `grep nexus-webapp-context-path= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one

	    context="$i"
	done

	#PORT="$(grep application-port $nexus2_dir/conf/nexus.properties)"
	#port_number=`echo $PORT | cut -f 2- -d = | tr -d '[[:space:]]'`
	#echo "${PORT} | href=http://localhost:${port_number}/nexus"

	for i in `grep application-port= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one

		if [ "8081" = "$i" ]
		then
		    echo "http (default): $i$context | href=http://localhost:$i$context"
		else

		    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
		    #echo "port: $i | bash='open http://localhost:$i && exit'"
		    echo "http: $i$context | href=http://localhost:$i$context"
		fi
	done

	for i in `grep application-port-ssl= $p_config_file | grep -v '#' | cut -f 2- -d = `
	do 
		rc=0 #indicate we found one

	    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
	    #echo "port: $i | bash='open http://localhost:$i && exit'"
	    echo "https: $i$context | href=https://localhost:$i$context"
	done

	return $rc
}

ps_cmd="ps aux | grep wrapper.conf | grep -v grep | sort"

instances=`eval $ps_cmd | wc -l | tr -d '[[:space:]]'`
[ $instances -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${BIT_BAR_MENU_TITLE}($instances) | color=$menubar_running_color"

instance_dirs=""

while read line
do
	# the while loop executes for some reason when line is zero (i.e. if there are no processes running)
    if [ -n "$line" ] 
    then
		echo "---"

	    wrapper_pid=`echo $line | awk '{print $2}'`
	    instance_line=`ps aux | grep "\-Dwrapper\.pid=$wrapper_pid"`
	    instance_pid=`echo $instance_line | awk '{print $2}'`

	    echo $instance_line | grep "bitbar-pwd" > /dev/null 2>&1
	    has_bitbar=$?

	    instance_dir=""
		if [ $has_bitbar -eq 0 ]
	    then
		    #finding the field that matches a string = http://www.tek-tips.com/viewthread.cfm?qid=1358154
		    instance_dir=`echo $instance_line | awk '{for(i=1;i<=NF;++i)if($i~/bitbar-pwd/)str=$i; sub("-Dbitbar-pwd=", "", str); print str}'` #wrapper.conf : -Dbitbar-pwd=/Users/mworthington/Downloads/nexus/nexus3-demo/current
		else
			echo "bitbar not configured in wrapper.conf. falling back to ps aux"
			#NOTE: This will be the absolute dir, not any symlinks
			instance_dir=`echo $line | awk '{str=$12; sub("\/bin\/\.\.\/bin\/jsw\/conf\/wrapper\.conf", "", str); print str}'`
		fi

		if [ -n "$instance_dir" ]
	    then
		    instance_dirs="$instance_dirs $instance_dir |"

			echo "$instance_dir ($wrapper_pid - $instance_pid) | color=green"

			find_ports_in_config_file $instance_dir/conf/nexus.properties

			#echo "---"
			echo "Log Dir | bash='cd $instance_dir/../sonatype-work/nexus/logs'"
			echo "-- request.log | bash='cd $instance_dir/../sonatype-work/nexus/logs && tail -F request.log'"
			echo "-- nexus.log | bash='cd $instance_dir/../sonatype-work/nexus/logs && tail -F nexus.log'"
			#echo "---"

			#echo "Stop Nexus 2 | color=purple bash='$instance_dir/bin/nexus stop && exit' terminal=true" #i can't figure out why a command with spaces won't run when terminal=false, so true and exit
			#echo "test4 Nexus 2 | color=purple bash=\"echo $dir >> ~/test.txt\" terminal=false"
			#echo "test7 | color=purple bash=echo param1=hello7 param2=>> param3=~/test.txt terminal=false"
			#echo "test4 | color=purple bash:'echo hello3 $dir >> ~/test.txt && exit' terminal=true"

			echo "Stop Nexus 2 | color=purple bash='/bin/bash' param1='-c' param2='$instance_dir/bin/nexus stop' terminal=false refresh=true"
			echo "Restart Nexus 2 | color=purple bash='/bin/bash' param1='-c' param2='$instance_dir/bin/nexus stop && while kill -0 $wrapper_pid 2> /dev/null; do sleep 1; done; $instance_dir/bin/nexus start' terminal=false refresh=true"
		else
			echo "No Dir for PID: $wrapper_pid - $instance_pid"
		fi

	fi
done <<< "$(eval $ps_cmd)" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered

echo "---"
#echo "instance_dirs: $instance_dirs"
#echo "---"

for (( i=0; i < ${#nexus2_dirs[@]}; i++ ))
do
	#this_base_dir=`dirname ${nexus2_dirs[$i]}`
	this_base_dir=${nexus2_dirs[$i]}

#	echo "this_base_dir: $this_base_dir"
	echo $instance_dirs | grep "$this_base_dir |" > /dev/null 2>&1
	if [ $? -gt 0 ]
	then
		#bitbar colors can use friendly name or hex code
		#https://getbitbar.com/plugins/Tutorial/ansi.sh
		echo "Start Nexus 2 (${this_base_dir}) | color=#123def bash='/bin/bash' param1='-c' param2='${this_base_dir}/bin/nexus start' terminal=false refresh=true"
		#echo "Start Nexus 3 (${this_base_dir}) | color=blue bash='${this_base_dir}/bin/nexus start && exit' terminal=true"
	fi
done
