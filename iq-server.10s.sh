#!/bin/bash
source /Users/mworthington/projects/GIT-Projects/bitbucket.org/michaelmworthington/bitbar-nexus-scripts/Scripts/config.cfg 

iq_demo_dir="/Users/mworthington/Downloads/iq-server/iq-server-demo"

#BIT_BAR_MENU_TITLE="IQ Server"
BIT_BAR_MENU_TITLE="IQ"

function find_ports_in_config_file ()
{
	local p_config_file=$1
	local rc=1
	local context="" #there is no default context for IQ Server

	#for my apache demo
	if [ "$iq_demo_dir/current/config.yml" == "$p_config_file" ]
	then
		#echo "Apache HTTPD: | color=orange"
		echo "Apache HTTPD: | color=#770000"
		echo "-- http: 80/server-status | href=http://127.0.0.1/server-status/"
		echo "-- http: 80/server-info | href=http://127.0.0.1/server-info/"
		echo "http: 80/iq | href=http://localhost/iq/"
		echo "http: 80/rutiq/rest/support | href=http://localhost/rutiq/rest/support?includeDb=true&noLimit=true"
		echo "http: 80/rutiq/rest/policy/export | href=http://localhost/rutiq/rest/policy/organization/ROOT_ORGANIZATION_ID/export"

		echo "http: 80/rutiq | href=http://localhost/rutiq/assets/index.html"
		echo "http: 80/firewall | href=http://localhost/firewall/"
		echo "Log Dir | bash='cd /var/log/apache2/'"
		echo "-- access_log | bash='cd /var/log/apache2/ && tail -F access_log'"
		echo "-- error_log | bash='cd /var/log/apache2/ && tail -F error_log'"
		echo "Conf Dir | bash='cd /etc/apache2/'"

		# H2
		echo "H2 (stops IQ): | color=#770000"
		echo "-- h2 shell - ods | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='ods'"
		echo "-- h2 web console - ods | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='ods' param4='TRUE'"
		echo "-- h2 web console - dm | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='dm' param4='TRUE'"
		echo "-- h2 web console - aggregation | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid' param3='aggregation' param4='TRUE'"

		#IQ Links
		echo "IQ Admin: | color=#770000"
		echo "-- http: 8061/healthcheck | href=http://localhost:8061/healthcheck"
		echo "-- http: 8061/metrics | href=http://localhost:8061/metrics?pretty=true"
		echo "-- http: 8061/tasks/backupDb | href=http://localhost:8061/tasks/backupDb" #TODO: make a POST
		echo "-- http: 8061/tasks/triggerPolicyMonitor | href=http://localhost:8061/tasks/triggerPolicyMonitor"
		echo "-- http: 8061/threads | href=http://localhost:8061/threads"
		echo "-- http: 8060/support (needs auth) | href=http://localhost:8060/iq/rest/support?includeDb=true&noLimit=true"


		#Firewall Links
		echo "Firewall (Supported): | color=#770000"
		echo "-- go-proxy | href=http://localhost/rutiq/assets/index.html#/repository/6b16768f1bfb4050873d6f01b81d2481/result"
		echo "-- maven-central | href=http://localhost/rutiq/assets/index.html#/repository/bd22030615004ef79dbf7881661f9f48/result"
		echo "-- npmjs.org-proxy | href=http://localhost/rutiq/assets/index.html#/repository/e3302e16eb4d4a749f3369b385d32571/result"
		echo "-- nuget.org-proxy | href=http://localhost/rutiq/assets/index.html#/repository/600a2b890de54242982082a2d02d4178/result"
		echo "-- pypy-python.org-proxy | href=http://localhost/rutiq/assets/index.html#/repository/0c7c45f4f3c14d8c95f61eacd1de35bb/result"
		echo "-- rubygems.org-releases | href=http://localhost/rutiq/assets/index.html#/repository/4792fb7fd8a242eb8583befc298b9ce8/result"
		echo "-- yum-epel | href=http://localhost/rutiq/assets/index.html#/repository/7b706cc05b4243579decb87ebf6a4d2d/result"
		#Firewall Links


		echo "Firewall (Un-Supported): | color=#770000"
		echo "-- apt | href=http://localhost/rutiq/assets/index.html#/repository/5f396b8de8f2429f93cf8adee5d172a3/result"
		echo "-- bower | href=http://localhost/rutiq/assets/index.html#/repository/6f703a9b41b5435582254cc16174940d/result"
		echo "-- cocoapods | href=http://localhost/rutiq/assets/index.html#/repository/9bc1840ef06a49de98ba970f0a3cdbc4/result"
		echo "-- conda | href=http://localhost/rutiq/assets/index.html#/repository/bb08e6466a114f9284287ea1e36ad750/result"
		echo "-- docker | href=http://localhost/rutiq/assets/index.html#/repository/a8afa742625f458488095ad218d77c2b/result"
		echo "-- raw-google-android-sdk | href=http://localhost/rutiq/assets/index.html#/repository/5fd9cb07257e40a5973df098aa2c22dd/result"
		echo "-- raw-anaconda | href=http://localhost/rutiq/assets/index.html#/repository/daab80d0de714c29b5f239be55a6d545/result"
		echo "-- raw-node-saas | href=http://localhost/rutiq/assets/index.html#/repository/d8783c9345494a22af4f328ac92d0107/result"

		#IQ UI
		echo "IQ Server: | color=#770000"
	fi

	for i in `grep applicationContextPath: $p_config_file | grep -v '#' | grep -v org. | cut -f 2- -d : `
	do 
		rc=0 #indicate we found one

	    context="$i"
	done

	#PORT="$(grep port $iq_dir/config.yml | grep -v '#' | grep -v org.)"
	#echo "${PORT}"
	for i in `grep port: $p_config_file | grep -v '#' | grep -v org. | cut -f 2- -d : `
	do 
		rc=0 #indicate we found one

		proto="http"
		for j in `grep "nonblocking+ssl" $p_config_file | grep -v '#'`
		do
			proto="https"
			#echo "https"
		done

		if [ "8070" = "$i" ]
		then
		    echo "$proto (default): $i$context | href=$proto://localhost:$i$context"
		else

		    #echo "port: $i | bash='open http://localhost:$i' terminal=false"
		    #echo "port: $i | bash='open http://localhost:$i && exit'"
		    echo "$proto: $i$context | href=$proto://localhost:$i$context"
		fi
	done

	return $rc
}

#ps aux | grep nexus-iq-server | grep -v grep >/dev/null 2>&1
#rc=$?
#echo "IQ: $rc"
#echo "---"
#[ $rc -gt 0 ] && echo "IQ Server | color=red" || echo "IQ Server | color=green"
ps_cmd="ps aux | grep nexus-iq-server | grep -v grep | grep -v demo.sh | sort"


#################################
# Bit Bar Title
#################################
instances=`eval $ps_cmd | wc -l | tr -d '[[:space:]]'`
# [ $instances -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=red" || echo "${BIT_BAR_MENU_TITLE}($instances) | color=green"
[ $instances -lt 1 ] && echo "$BIT_BAR_MENU_TITLE | color=$menubar_stopped_color" || echo "${BIT_BAR_MENU_TITLE}($instances) | color=$menubar_running_color"

#################################
# Active IQ Servers
#################################
instance_dirs=""

while read line
do
	# the while loop executes for some reason when line is zero (i.e. if there are no processes running)
    if [ -n "$line" ] 
    then
	    echo $line | grep "bitbar-pwd" > /dev/null 2>&1
	    has_bitbar=$?

	    instance_pid=`echo $line | awk '{print $2}'`
	    #str=$12
   	    #finding the field that matches a string = http://www.tek-tips.com/viewthread.cfm?qid=1358154
	    instance_dir=`echo $line | awk '{for(i=1;i<=NF;++i)if($i~/bitbar-pwd/)str=$i; sub("-Dbitbar-pwd=", "", str); print str}'`
	    instance_dirs="$instance_dirs $instance_dir"

		echo "---"
		
		if [ $has_bitbar -eq 0 ]
	    then
			echo "$instance_dir ($instance_pid) | color=green"

			find_ports_in_config_file "$instance_dir/config.yml"

			log_dir=$instance_dir/log
			if [ "$iq_demo_dir/current" == "$instance_dir" ]
			then
			  log_dir=$instance_dir/../sonatype-work/clm-server/log
			fi

			#echo "---"
			echo "Log Dir | bash='cd $log_dir && ls -laGi *.log'"
			echo "-- request.log | bash='cd $log_dir && tail -F request.log'"
			echo "-- audit.log | bash='cd $log_dir && tail -F audit.log'"
			echo "-- policy-violation.log | bash='cd $log_dir && tail -F policy-violation.log'"
			echo "-- clm-server.log | bash='cd $log_dir && tail -F clm-server.log'"
			#echo "---"
		else
			echo "No Dir for PID: $instance_pid"
		fi

		#echo "Stop IQ Server | color=purple bash='kill $instance_pid && exit' terminal=true refresh=true"
		echo "Stop IQ Server | color=purple bash='/bin/bash' param1='-c' param2='kill $instance_pid' terminal=false refresh=true"
		#echo "Restart IQ Server | color=purple bash='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/demo.sh > /dev/null 2>&1 & exit' terminal=true refresh=true"
		#echo "Restart IQ Server | color=purple bash='/bin/bash' param1='-c' param2='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/demo.sh > /dev/null 2>&1 & exit' terminal=false refresh=true"
		echo "Restart IQ Server | color=purple bash='/bin/bash' param1='-c' param2='kill $instance_pid && while kill -0 $instance_pid 2> /dev/null; do sleep 1; done; $instance_dir/demo.sh > $instance_dir/demo.log 2>&1 & exit' terminal=false refresh=true"
	fi
done <<< "$(eval $ps_cmd)" #need to have this down here because of http://stackoverflow.com/questions/16854280/modifying-variable-inside-while-loop-is-not-remembered


echo "---"

#################################
# Stopped IQ Servers
#################################
for (( i=0; i < ${#iq_dirs[@]}; i++ ))
do
	this_base_dir=`dirname ${iq_dirs[$i]}`

	#echo "---"
	#echo "instance dirs: $instance_dirs"
	#echo "---"
	#echo "this dir: $this_base_dir"
	#echo "this dir: ${iq_dirs[$i]}"
	#echo "---"


	# not sure why i had this going against the base dir. for now comment out and go back against the regular dir
	#            ---- I think I know, because current symlink was getting resolved, but that doesn't look like the case now
	#echo $instance_dirs | grep "$this_base_dir" > /dev/null 2>&1
	echo $instance_dirs | grep "${iq_dirs[$i]}" > /dev/null 2>&1
	if [ $? -gt 0 ]
	then
		#echo "Start IQ Server (${iq_dirs[$i]}) | color=#123def bash='/bin/bash' param1='-c' param2='${iq_dirs[$i]}/demo.sh > /dev/null 2>&1 & exit' terminal=false refresh=true"
		echo "Start IQ Server (${iq_dirs[$i]}) | color=#123def bash='/bin/bash' param1='-c' param2='${iq_dirs[$i]}/demo.sh > ${iq_dirs[$i]}/demo.log 2>&1 & exit' terminal=false refresh=true"
		#echo "Start IQ Server (${iq_dirs[$i]}) | color=#123def bash='${iq_dirs[$i]}/demo.sh > /dev/null 2>&1 & exit' terminal=true refresh=true"
	fi
done

echo "---"

#How to put quotes in the bash command
#        https://github.com/matryer/bitbar/issues/407
#        https://github.com/matryer/bitbar/issues/165
#iq_demo_dir="/Users/mworthington/Downloads/iq-server/iq-server-demo"
#instance_pid=""
#echo "h2 shell (10) |  terminal=true bash='/bin/bash' param1='-c' param2='cd $iq_demo_dir/sonatype-work/clm-server/data && java -cp $iq_demo_dir/current/nexus-iq-server*.jar org.h2.tools.Shell -user sa -url' param3="'jdbc:h2:./ods;DATABASE_TO_UPPER=FALSE' '"
#echo "h2 shell (1) | terminal=true bash='$iq_demo_dir/h2-shell-ods.sh' param1='$iq_demo_dir' param2='$instance_pid'"


#################################
# Testing
#################################
#iq_demo_dir="/Users/mworthington/Downloads/iq-server/iq-server-demo"
#echo "h2 shell (4) | bash='cd $iq_demo_dir/sonatype-work/clm-server/data && java -cp $iq_demo_dir/current/nexus-iq-server*.jar org.h2.tools.Shell -user sa -url jdbc:h2:./ods;DATABASE_TO_UPPER=FALSE'"

#echo "hello | bash='cd ~/Downloads'"
#echo "Option key is pressed | alternate=true bash='cd ~/Desktop'"

#echo "Another"

#i can't figure out why a command with spaces won't run when terminal=false, so true and exit
# solution: https://github.com/matryer/bitbar/issues/407
#my_date=`date "+%Y%m%d-%H%M%S"`
#echo "test terminal true (1)  | color=purple bash='echo $my_date >> ~/test.txt' terminal=true" 
#echo "test terminal false (2) | color=purple bash='/bin/bash' param1='-c' param2='echo $my_date >> ~/test.txt' terminal=false"
#echo "test7 | color=purple bash=echo param1=hello7 param2=>> param3=~/test.txt terminal=false"
#echo "test4 | color=purple bash:'echo hello3 $instance_dir >> ~/test.txt && exit' terminal=true"
