#!/usr/bin/env bash
BASEDIR=$(dirname "$0")

#source ~/.bashrc
source $BASEDIR/config.cfg

nohup $nexus2_dir/bin/nexus start > /dev/null 2>&1 &
